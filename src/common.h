#ifndef __COMMON_H__
#define __COMMON_H__
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef uint32_t uint;
typedef uint16_t ushort;
typedef uint64_t ulong;
typedef uint64_t lbaint_t;
typedef u64 phys_size_t;
typedef uint8_t uchar;
typedef uint32_t __u32;
typedef uint32_t __be32;

#define LBAF "%lx"
#define LBAFU "%lu"

#define SD_DEBUG_ENABLE 1
#ifdef SD_DEBUG_ENABLE
    #define sd_debug(a...) debug(a);
#else
    #define sd_debug(a...)
#endif

#define EMMC_DEBUG_ENABLE 1
#ifdef EMMC_DEBUG_ENABLE
    #define emmc_debug(a...) debug(a);
#else
    #define emmc_debug(a...)
#endif

#define CONFIG_MMC_TRACE 1

#define CONFIG_ENV_IS_NOWHERE
#define CONFIG_AML_SD_EMMC
#define CONFIG_GENERIC_MMC 1
#define	CONFIG_SYS_MMC_ENV_DEV 1
#define CONFIG_EMMC_DDR52_EN 0

#define __deprecated   __attribute__((deprecated))
#ifndef __packed
#define __packed       __attribute__((packed))
#endif
#ifndef __weak
#define __weak         __attribute__((weak))
#endif
#define __force

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#define __swab32(x) \
    ((__u32)( \
    (((__u32)(x) & (__u32)0x000000ffUL) << 24) | \
    (((__u32)(x) & (__u32)0x0000ff00UL) <<  8) | \
    (((__u32)(x) & (__u32)0x00ff0000UL) >>  8) | \
    (((__u32)(x) & (__u32)0xff000000UL) >> 24) ))

#define __be32_to_cpu(x) __swab32((__force __u32)(__be32)(x))


extern uint32_t aml_rreg32(uint32_t addr);
extern void aml_wreg32(uint32_t addr, uint32_t val);

void clrbits_le32(uint32_t addr, uint32_t val);
void setbits_le32(uint32_t addr, uint32_t val);
uint32_t readl(uint32_t addr);
void writel(uint32_t addr, uint32_t val);
uint32_t get_timer(uint32_t x);

#define MESON_CPU_MAJOR_ID_M6		0x16
#define MESON_CPU_MAJOR_ID_M6TV		0x17
#define MESON_CPU_MAJOR_ID_M6TVL	0x18
#define MESON_CPU_MAJOR_ID_M8		0x19
#define MESON_CPU_MAJOR_ID_MTVD		0x1A
#define MESON_CPU_MAJOR_ID_M8B		0x1B
#define MESON_CPU_MAJOR_ID_MG9TV	0x1C
#define MESON_CPU_MAJOR_ID_M8M2		0x1D
#define MESON_CPU_MAJOR_ID_GXBB		0x1F
#define MESON_CPU_MAJOR_ID_GXTVBB	0x20
#define MESON_CPU_MAJOR_ID_GXL		0x21
#define MESON_CPU_MAJOR_ID_GXM		0x22
#define MESON_CPU_MAJOR_ID_TXL		0x23

#define MESON_CPU_PACKAGE_ID_905D	0X00
#define MESON_CPU_PACKAGE_ID_905M	0x20
#define MESON_CPU_PACKAGE_ID_905X	0X80
#define MESON_CPU_PACKAGE_ID_905L	0XC0
#define MESON_CPU_PACKAGE_ID_905M2	0XE0

#define MESON_CPU_CHIP_REVISION_A	0xA
#define MESON_CPU_CHIP_REVISION_B	0xB
#define MESON_CPU_CHIP_REVISION_C	0xC
#define MESON_CPU_CHIP_REVISION_D	0xD

typedef struct cpu_id {
	unsigned int family_id:8; //S905/T968 etc.
	unsigned int package_id:8; //T968/T966 etc.
	unsigned int chip_rev:8; //RevA/RevB etc.
	unsigned int reserve:4;
	unsigned int layout_ver:4;
} cpu_id_t;

cpu_id_t get_cpu_id(void);
void debug(const char *fmt, ...);
void set_debug(bool sw);

#endif
