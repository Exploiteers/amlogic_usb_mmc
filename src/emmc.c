#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "common.h"
#include "secure_apb.h"
#include "cpu_sdio.h"
#include "sd_emmc.h"
#include "mmc.h"

static unsigned sd_debug_board_1bit_flag = 0;

int aml_sd_send_cmd(struct mmc *mmc, struct mmc_cmd *cmd, struct mmc_data *data);
void aml_sd_cfg_swth(struct mmc *mmc);

unsigned long sd_emmc_base_addr[3] = {SD_EMMC_BASE_A,
                                      SD_EMMC_BASE_B,
                                      SD_EMMC_BASE_C};

static struct aml_card_sd_info aml_sd_emmc_ports[]={
    { .sd_emmc_port=SDIO_PORT_A,.name="SDIO Port A"},
    { .sd_emmc_port=SDIO_PORT_B,.name="SDIO Port B"},
    { .sd_emmc_port=SDIO_PORT_C,.name="SDIO Port C"},
};

struct aml_card_sd_info * cpu_sd_emmc_get(unsigned port)
{
    if (port<SDIO_PORT_C+1)
        return &aml_sd_emmc_ports[port];
    return NULL;
}


static int sd_inand_check_insert(struct    mmc    *mmc)
{
    int level;
    struct aml_card_sd_info *sd_inand_info = mmc->priv;

    level = sd_inand_info->sd_emmc_detect(sd_inand_info->sd_emmc_port);

    if (level) {
        if (sd_inand_info->init_retry) {
            sd_inand_info->sd_emmc_pwr_off(sd_inand_info->sd_emmc_port);
            sd_inand_info->init_retry = 0;
        }
        if (sd_inand_info->inited_flag) {
            sd_inand_info->sd_emmc_pwr_off(sd_inand_info->sd_emmc_port);
            sd_inand_info->removed_flag = 1;
            sd_inand_info->inited_flag = 0;
        }
        return 0;                //No card is inserted
    } else {
        return 1;                //A    card is    inserted
    }
}

static int sd_inand_staff_init(struct mmc *mmc)
{
    struct aml_card_sd_info * sdio=mmc->priv;
    unsigned base;

    sd_debug("power prepare");
    sdio->sd_emmc_pwr_prepare(sdio->sd_emmc_port);
    sd_debug("power off");
    sdio->sd_emmc_pwr_off(sdio->sd_emmc_port);

    //try to init mmc controller clock firstly
    mmc->clock = 400000;
    aml_sd_cfg_swth(mmc);

    if (sdio->sd_emmc_port == SDIO_PORT_B) {  //only power ctrl for external tf card
        base=get_timer(0);
#if defined(CONFIG_VLSI_EMULATOR)
        while (get_timer(base)<1) ;
#else
        while (get_timer(base)<200) ;
#endif
    }
    sdio->sd_emmc_pwr_on(sdio->sd_emmc_port);
    sdio->sd_emmc_init(sdio->sd_emmc_port);
    if (sd_debug_board_1bit_flag == 1) {
        struct mmc_config *cfg;
        cfg = &((struct aml_card_sd_info *)mmc->priv)->cfg;
        cfg->host_caps = MMC_MODE_HS;
        mmc->cfg = cfg;
    }

    if (sdio->sd_emmc_port == SDIO_PORT_B) {   //only power ctrl for external tf card
        base=get_timer(0);
#if defined(CONFIG_VLSI_EMULATOR)
        while (get_timer(base)<1) ;
#else
        while (get_timer(base)<200) ;
#endif
    }
    if (!sdio->inited_flag)
        sdio->inited_flag = 1;
    return SD_NO_ERROR;
}

int aml_sd_init(struct mmc *mmc)
{
    struct aml_card_sd_info *sdio=mmc->priv;

    if (sdio->inited_flag) {
        sdio->sd_emmc_init(sdio->sd_emmc_port);
        mmc->cfg->ops->set_ios(mmc);
        return 0;
    }

    if (sd_inand_check_insert(mmc)) {
        sd_inand_staff_init(mmc);
        return 0;
    } else
        return 1;
}


static const struct mmc_ops aml_sd_emmc_ops = {
    .send_cmd    = aml_sd_send_cmd,
    .set_ios    = aml_sd_cfg_swth,
    .init          = aml_sd_init,
//    .getcd        = ,
//    .getwp        = ,
//    .calibration = aml_sd_calibration,
//    .refix = aml_sd_retry_refix,
};

void sd_emmc_register(struct aml_card_sd_info * aml_priv)
{
    struct mmc_config *cfg;

    aml_priv->removed_flag = 1;
    aml_priv->inited_flag = 0;
    aml_priv->sd_emmc_reg = (struct sd_emmc_global_regs *)sd_emmc_base_addr[aml_priv->sd_emmc_port];

    cfg = &aml_priv->cfg;
    cfg->name = aml_priv->name;
    cfg->ops = &aml_sd_emmc_ops;

    cfg->voltages = MMC_VDD_33_34|MMC_VDD_32_33|MMC_VDD_31_32|MMC_VDD_165_195;
    cfg->host_caps = MMC_MODE_8BIT|MMC_MODE_4BIT | MMC_MODE_HS_52MHz | MMC_MODE_HS |
#if CONFIG_EMMC_DDR52_EN
                 MMC_MODE_HC | MMC_MODE_DDR_52MHz;
#else
                 MMC_MODE_HC;
#endif
    cfg->f_min = 400000;
    cfg->f_max = 12000000; //@40Mhz in DFU, the AFTV cube reset it self
    cfg->part_type = PART_TYPE_AML;
    cfg->b_max = 1; //256;  set to 1 block because we can't do DMA yet since DRAM isn't init.
    mmc_create(cfg,aml_priv);
}

static int  sd_emmc_init(unsigned port)
{
    switch (port)
    {
        case SDIO_PORT_A:
            break;
        case SDIO_PORT_B:
            //todo add card detect
            //setbits_le32(P_PREG_PAD_GPIO5_EN_N,1<<29);//CARD_6
            break;
        case SDIO_PORT_C:
            //enable pull up
            //clrbits_le32(P_PAD_PULL_UP_REG3, 0xff<<0);
            break;
        default:
            break;
    }

    return cpu_sd_emmc_init(port);
}

static int  sd_emmc_detect(unsigned port)
{
    uint32_t ret;

    switch (port) {

    case SDIO_PORT_A:
            break;
    case SDIO_PORT_B:
        setbits_le32(P_PREG_PAD_GPIO2_EN_N, 1 << 26);//CARD_6
        ret = readl(P_PREG_PAD_GPIO2_I) & (1 << 26) ? 0 : 1;
        printf("%s\n", ret ? "card in" : "card out");
        if ((readl(P_PERIPHS_PIN_MUX_6) & (3 << 8))) { //if uart pinmux set, debug board in
            if (!(readl(P_PREG_PAD_GPIO2_I) & (1 << 24))) {
                printf("sdio debug board detected, sd card with 1bit mode\n");
                sd_debug_board_1bit_flag = 1;
            } else{
                printf("sdio debug board detected, no sd card in\n");
                sd_debug_board_1bit_flag = 0;
                return 1;
            }
        }
        break;
    default:
        break;
    }
    return 0;
}


static void sd_emmc_pwr_prepare(unsigned port)
{
    cpu_sd_emmc_pwr_prepare(port);
}

static void sd_emmc_pwr_on(unsigned port)
{
    switch (port)
    {
        case SDIO_PORT_A:
            break;
        case SDIO_PORT_B:
//            clrbits_le32(P_PREG_PAD_GPIO5_O,(1<<31)); //CARD_8
//            clrbits_le32(P_PREG_PAD_GPIO5_EN_N,(1<<31));
            /// @todo NOT FINISH
            break;
        case SDIO_PORT_C:
            break;
        default:
            break;
    }
    return;
}
static void sd_emmc_pwr_off(unsigned port)
{
    /// @todo NOT FINISH
    switch (port)
    {
        case SDIO_PORT_A:
            break;
        case SDIO_PORT_B:
//            setbits_le32(P_PREG_PAD_GPIO5_O,(1<<31)); //CARD_8
//            clrbits_le32(P_PREG_PAD_GPIO5_EN_N,(1<<31));
            break;
        case SDIO_PORT_C:
            break;
                default:
            break;
    }
    return;
}

static void board_mmc_register(unsigned port)
{
    struct aml_card_sd_info *aml_priv=cpu_sd_emmc_get(port);
    if (aml_priv == NULL)
        return;

    aml_priv->sd_emmc_init=sd_emmc_init;
    aml_priv->sd_emmc_detect=sd_emmc_detect;
    aml_priv->sd_emmc_pwr_off=sd_emmc_pwr_off;
    aml_priv->sd_emmc_pwr_on=sd_emmc_pwr_on;
    aml_priv->sd_emmc_pwr_prepare=sd_emmc_pwr_prepare;
    aml_priv->desc_buf = malloc(NEWSD_MAX_DESC_MUN*(sizeof(struct sd_emmc_desc_info)));

    if (NULL == aml_priv->desc_buf)
        printf(" desc_buf Dma alloc Fail!\n");
    else
        printf("aml_priv->desc_buf = 0x%p\n",aml_priv->desc_buf);

    sd_emmc_register(aml_priv);
}

int board_mmc_init(bd_t    *bis)
{
    struct mmc *mmc;
#ifdef CONFIG_VLSI_EMULATOR
    //board_mmc_register(SDIO_PORT_A);
#else
    //board_mmc_register(SDIO_PORT_B);
#endif
    board_mmc_register(SDIO_PORT_B);
    board_mmc_register(SDIO_PORT_C);
//    board_mmc_register(SDIO_PORT_B1);
#if defined(CONFIG_ENV_IS_NOWHERE) && defined(CONFIG_AML_SD_EMMC)
    /* try emmc here. */
    mmc = find_mmc_device(CONFIG_SYS_MMC_ENV_DEV);
    if (!mmc)
        printf("%s() %d: No MMC found\n", __func__, __LINE__);
    else if (mmc_init(mmc))
        printf("%s() %d: MMC init failed\n", __func__, __LINE__);
#endif
    return 0;
}

void aml_sd_cfg_swth(struct mmc *mmc)
{

    unsigned sd_emmc_clkc =    0,clk,clk_src,clk_div = 0;
    unsigned vconf;
    unsigned bus_width=(mmc->bus_width == 1)?0:mmc->bus_width/4;
    struct aml_card_sd_info *aml_priv = mmc->priv;
    struct sd_emmc_global_regs *sd_emmc_reg = aml_priv->sd_emmc_reg;
    struct sd_emmc_config* sd_emmc_cfg = (struct sd_emmc_config*)&vconf;

    emmc_debug("mmc->clock=%d; clk_div=%d\n",mmc->clock ,clk_div);

    /* reset gdelay , gadjust register */
    aml_wreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gdelay, 0);
    aml_wreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gadjust, 0);
    //sd_emmc_reg->gdelay = 0;
    //sd_emmc_reg->gadjust = 0;

    if(mmc->clock > 12000000) {
        clk = SD_EMMC_CLKSRC_DIV2;
        clk_src = 1;
    }else{
        clk = SD_EMMC_CLKSRC_24M;
        clk_src = 0;
    }

    if (mmc->clock<mmc->cfg->f_min)
        mmc->clock=mmc->cfg->f_min;
    if (mmc->clock>mmc->cfg->f_max)
        mmc->clock=mmc->cfg->f_max;

    clk_div= clk / mmc->clock;
#if CONFIG_EMMC_DDR52_EN
    if (mmc->ddr_mode) {
        clk_div /= 2;
        sd_debug("DDR: \n");
    }
#endif

    sd_emmc_clkc =((0 << Cfg_irq_sdio_sleep_ds) |
                  (0 << Cfg_irq_sdio_sleep)     |
                  (1 << Cfg_always_on)          |
                  (0 << Cfg_rx_delay)           |
                  (0 << Cfg_tx_delay)           |
                  (0 << Cfg_sram_pd)            |
                  (0 << Cfg_rx_phase)           |
                  (0 << Cfg_tx_phase)           |
                  (2 << Cfg_co_phase)           |
                  (clk_src << Cfg_src)          |
                  (clk_div << Cfg_div));

    aml_wreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gclock, sd_emmc_clkc);
    vconf = aml_rreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gcfg);
    //sd_emmc_reg->gclock = sd_emmc_clkc;
    //vconf = sd_emmc_reg->gcfg;

    sd_emmc_cfg->bus_width = bus_width;     //1bit mode
    sd_emmc_cfg->bl_len = 9;      //512byte block length
    sd_emmc_cfg->resp_timeout = 7;      //64 CLK cycle, here 2^8 = 256 clk cycles
    sd_emmc_cfg->rc_cc = 4;      //1024 CLK cycle, Max. 100mS.
#if CONFIG_EMMC_DDR52_EN
    sd_emmc_cfg->ddr = mmc->ddr_mode;
#endif
    //sd_emmc_reg->gcfg = vconf;
    aml_wreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gcfg, vconf);

    emmc_debug("bus_width=%d; tclk_div=%d; tclk=%d;sd_clk=%d\n",
        bus_width,clk_div,clk,mmc->clock);
    emmc_debug("port=%d act_clk=%d\n",aml_priv->sd_emmc_port,clk/clk_div);
    return;
}


//Clear response data buffer
static void sd_inand_clear_response(unsigned * res_buf)
{
    int i;
    if (res_buf == 0)
        return;

    for (i = 0; i < MAX_RESPONSE_BYTES; i++)
        res_buf[i]=0;
}

/*
 * Sends a command out on the bus. Takes the mmc pointer,
 * a command pointer, and an optional data pointer.
 */
int aml_sd_send_cmd(struct mmc *mmc, struct mmc_cmd *cmd, struct mmc_data *data)
{
    int ret = SD_NO_ERROR;
    //u32 vconf;
    u32 resp_buffer;
    //u32 vstart = 0;
    u32 status_irq = 0;
    //u32 inalign = 0;
    struct sd_emmc_status *status_irq_reg = (void *)&status_irq;
    //struct sd_emmc_start *desc_start = (struct sd_emmc_start*)&vstart;
    //struct sd_emmc_config* sd_emmc_cfg = (struct sd_emmc_config*)&vconf;
    struct aml_card_sd_info *aml_priv = mmc->priv;
    struct sd_emmc_global_regs *sd_emmc_reg = aml_priv->sd_emmc_reg;
    struct cmd_cfg *des_cmd_cur = NULL;
    struct sd_emmc_desc_info *desc_cur = (struct sd_emmc_desc_info*)aml_priv->desc_buf;
    unsigned i;

    memset(desc_cur, 0, sizeof(struct sd_emmc_desc_info));

    des_cmd_cur = (struct cmd_cfg *)&(desc_cur->cmd_info);
    des_cmd_cur->cmd_index = 0x80 | cmd->cmdidx; //bit:31 owner = 1 bit:24-29 cmdidx
    desc_cur->cmd_arg = cmd->cmdarg;

    sd_inand_clear_response(cmd->response);

    //check response type
    if (cmd->resp_type & MMC_RSP_PRESENT) {
        resp_buffer = (unsigned long)cmd->response;//dma_map_single((void*)cmd->response,sizeof(uint)*4,DMA_FROM_DEVICE);
        des_cmd_cur->no_resp = 0;

        //save Resp into Resp addr, and check response from register for RSP_136
        if (cmd->resp_type & MMC_RSP_136)
            des_cmd_cur->resp_128 = 1;

        if (cmd->resp_type & MMC_RSP_BUSY)
            des_cmd_cur->r1b = 1;    //check data0 busy after R1 reponse

        if (!(cmd->resp_type & MMC_RSP_CRC))
            des_cmd_cur->resp_nocrc = 1;

        des_cmd_cur->resp_num = 0;
        desc_cur->resp_addr = resp_buffer;
    }else
        des_cmd_cur->no_resp = 1;

    if (data) {
        if (data->blocks*data->blocksize <= 0x200) {
            desc_cur->data_addr = (unsigned long)sd_emmc_reg->gping;
            desc_cur->data_addr |= 1<<0;
        }
        else {
            printf("data size: %d\n", data->blocks*data->blocksize);
            printf("Error FIXXXXX MEE\n");
            exit(1);
        }

        des_cmd_cur->data_io = 1; // cmd has data read or write
        if (data->flags == MMC_DATA_READ) {
            des_cmd_cur->data_wr = 0;  //read data from sd/emmc
        }else{
            des_cmd_cur->data_wr = 1;
            for(i=0; i<data->blocks*data->blocksize; i+=4)
            {
              aml_wreg32(((uint32_t)(uintptr_t)&sd_emmc_reg->gping) + i, *(uint32_t*)(data->dest + i));
            }
        }

        if (data->blocks > 1) {
                des_cmd_cur->block_mode = 1;
                des_cmd_cur->length = data->blocks;
        }else{
                des_cmd_cur->block_mode = 0;
                des_cmd_cur->length = data->blocksize;
        }
        des_cmd_cur->data_num = 0;
    }
    /*Prepare desc for config register*/
    des_cmd_cur->owner = 1;
    des_cmd_cur->end_of_chain = 0;

    des_cmd_cur->end_of_chain = 1; //the end flag of descriptor chain

    aml_wreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gstatus, NEWSD_IRQ_ALL);
    //sd_emmc_reg->gstatus = NEWSD_IRQ_ALL;

    //invalidate_dcache_range((unsigned long)aml_priv->desc_buf,
    //                (unsigned long)(aml_priv->desc_buf+NEWSD_MAX_DESC_MUN*(sizeof(struct sd_emmc_desc_info))));
#if 0
    //start transfer cmd
    desc_start->init = 0;
    desc_start->busy = 1;
    desc_start->addr = 0;//FIXME!!//(unsigned long)aml_priv->desc_buf >> 2;

    sd_emmc_reg->gstart = vstart;
#else
    aml_wreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gcmd_cfg, desc_cur->cmd_info);
    aml_wreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gcmd_dat, desc_cur->data_addr);
    aml_wreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gcmd_arg, desc_cur->cmd_arg);
    // sd_emmc_reg->gcmd_cfg = desc_cur.cmd_info;
    // sd_emmc_reg->gcmd_dat = desc_cur.data_addr;
    // sd_emmc_reg->gcmd_arg = desc_cur.cmd_arg;
#endif
    //waiting end of chain
    //mmc->refix = 0;
    while (1) {
        //status_irq = sd_emmc_reg->gstatus;
        status_irq = aml_rreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gstatus);
        if (status_irq_reg->end_of_chain)
                break;
    }
    unsigned refix = 1;
    if (status_irq_reg->rxd_err) {
        ret |= SD_EMMC_RXD_ERROR;
        if (refix)
            printf("emmc/sd read error, cmd%d, status=0x%x\n",
                cmd->cmdidx, status_irq);
    }
    if (status_irq_reg->txd_err) {
        ret |= SD_EMMC_TXD_ERROR;
        if (refix)
            printf("emmc/sd write error, cmd%d, status=0x%x\n",
                cmd->cmdidx, status_irq);
    }
    if (status_irq_reg->desc_err) {
        ret |= SD_EMMC_DESC_ERROR;
        if (refix)
            printf("emmc/sd descripter error, cmd%d, status=0x%x\n",
                cmd->cmdidx, status_irq);
    }
    if (status_irq_reg->resp_err) {
        ret |= SD_EMMC_RESP_CRC_ERROR;
        if (refix)
            printf("emmc/sd response crc error, cmd%d, status=0x%x\n",
                cmd->cmdidx, status_irq);
    }
    if (status_irq_reg->resp_timeout) {
        ret |= SD_EMMC_RESP_TIMEOUT_ERROR;
        if (refix)
            printf("emmc/sd response timeout, cmd%d, status=0x%x\n",
                cmd->cmdidx, status_irq);
    }
    if (status_irq_reg->desc_timeout) {
        ret |= SD_EMMC_DESC_TIMEOUT_ERROR;
        if (refix)
            printf("emmc/sd descripter timeout, cmd%d, status=0x%x\n",
                cmd->cmdidx, status_irq);
    }
    if (data) {
        if ((data->blocks*data->blocksize <= 0x200) && (data->flags == MMC_DATA_READ)) {
            for(i=0; i<data->blocks*data->blocksize; i+=4)
            {
                *(uint32_t*)(data->dest + i) = aml_rreg32(((uint32_t)(uintptr_t)&sd_emmc_reg->gping) + i);
                //printf("[%d] = %08x\n", i, *(uint32_t*)(data->dest + i));
            }
            //memcpy(data->dest, (const void *)sd_emmc_reg->gping,data->blocks*data->blocksize);
        }
    }
    /*we get response [0]:bit0~31
     *        response [1]:bit32~63
     *        response [2]:bit64~95
     *        response [3]:bit96~127
     * actually mmc driver definition is:
     *         response [0]:bit96~127
     *        response [1]:bit64~95
     *        response [2]:bit32~63
     *        response [3]:bit0~31
     */

    if (cmd->resp_type & MMC_RSP_136) {
        cmd->response[0] = aml_rreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gcmd_rsp3);//sd_emmc_reg->gcmd_rsp3;
        cmd->response[1] = aml_rreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gcmd_rsp2);//sd_emmc_reg->gcmd_rsp2;
        cmd->response[2] = aml_rreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gcmd_rsp1);//sd_emmc_reg->gcmd_rsp1;
        cmd->response[3] = aml_rreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gcmd_rsp0);//sd_emmc_reg->gcmd_rsp0;
    } else {
        cmd->response[0] = aml_rreg32((uint32_t)(uintptr_t)&sd_emmc_reg->gcmd_rsp0);//sd_emmc_reg->gcmd_rsp0;
    }


    sd_debug("cmd->cmdidx = %d, cmd->cmdarg=0x%x, ret=0x%x\n",cmd->cmdidx,cmd->cmdarg,ret);
    sd_debug("cmd->response[0]=0x%x;\n",cmd->response[0]);
    sd_debug("cmd->response[1]=0x%x;\n",cmd->response[1]);
    sd_debug("cmd->response[2]=0x%x;\n",cmd->response[2]);
    sd_debug("cmd->response[3]=0x%x;\n",cmd->response[3]);

    if (ret) {
        if (status_irq_reg->resp_timeout)
            return TIMEOUT;
        else
            return ret;
    }

    return SD_NO_ERROR;
}

void cpu_sd_emmc_pwr_prepare(unsigned port)
{
    /** here you can add io pin voltage operation if you need.
        but now it was used to open emmc gate for gxm
    */
    if ((port == SDIO_PORT_C)
        && (get_cpu_id().family_id >= MESON_CPU_MAJOR_ID_GXM)
        && (readl(HHI_NAND_CLK_CNTL) != 0x80))
        writel(0x80, HHI_NAND_CLK_CNTL);
}

int cpu_sd_emmc_init(unsigned port)
{

    //printf("inand sdio  port:%d\n",port);
    switch (port)
    {
    case SDIO_PORT_A:
        setbits_le32(P_PERIPHS_PIN_MUX_5, (0x3f << 26) | (0x1 << 24));
        break;
    case SDIO_PORT_B:
        if (sd_debug_board_1bit_flag == 1)
            setbits_le32(P_PERIPHS_PIN_MUX_6, 0x7 << 2);
        else {
            clrbits_le32(P_PERIPHS_PIN_MUX_6, 0x3f << 6);
            setbits_le32(P_PERIPHS_PIN_MUX_6, 0x3f << 0);
        }
        break;
    case SDIO_PORT_C://SDIOC GPIOB_2~GPIOB_7
        clrbits_le32(P_PERIPHS_PIN_MUX_7, (0x7 << 5) | (0xff << 16));
        setbits_le32(P_PERIPHS_PIN_MUX_7, 0x7 << 29);
        break;
    default:
        return -1;
    }
    return 0;
}
