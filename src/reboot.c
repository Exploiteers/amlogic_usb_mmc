#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <libusb-1.0/libusb.h>
#include <getopt.h>
#include <endian.h>

#define P_WATCHDOG_CNTL              0xc11098d0
#define P_WATCHDOG_RESET             0xc11098dc
#define AO_RTI_STATUS_REG3           (0xc8100000 + (0x07 << 2))

/*
Reboot reason AND corresponding env setting:
0:  Cold boot                 cold_boot
1:  Normal boot               normal
2:  Factory reset             factory_reset
3:  Upgrade system            update
4:  Fastboot                  fastboot
5:  Suspend                   suspend_off
6:  Hibernate                 hibernate
7:  Fastboot Bootloader       bootloader
8:  reserved
9:  RPMBP provisioning        rpmbp
10: reserved
11:  Crash dump               crash_dump
12:  Kernel panic             kernel_panic
13:  Watchdog reboot          watchdog_reboot
14~15: reserved
*/
#define AMLOGIC_COLD_BOOT              0
#define AMLOGIC_NORMAL_BOOT             1
#define AMLOGIC_FACTORY_RESET_REBOOT    2
#define AMLOGIC_UPDATE_REBOOT           3
#define AMLOGIC_FASTBOOT_REBOOT         4
#define AMLOGIC_SUSPEND_REBOOT          5
#define AMLOGIC_HIBERNATE_REBOOT        6
#define AMLOGIC_BOOTLOADER_REBOOT       7 /* fastboot bootloader */
#define AMLOGIC_RPMBP_REBOOT            9
#define AMLOGIC_CRASH_REBOOT            11
#define AMLOGIC_KERNEL_PANIC            12
#define AMLOGIC_WATCHDOG_REBOOT         13


static struct libusb_device_handle *devh = NULL;
static int g_print_libusb_error = 1;

static int aml_rreg(uint32_t addr, uint32_t len, void* data)
{
    int status;

    status = libusb_control_transfer(devh,
        LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE,
        0x02,
        (addr >> 16) & 0xffff,
        addr & 0xffff,
        data,
        len,
        1000);

    if (status < 0) {
        if(g_print_libusb_error)
            fprintf(stderr, "Error sending control packet: %s\n", libusb_error_name(status));
        return -1;
    }
    else if (status != len) {
        if(g_print_libusb_error)
            fprintf(stderr, "Error invalid len: %d\n", status);
        return -2;
    }

    return 0;
}

static int aml_wreg(uint32_t addr, uint32_t len, void* data)
{
    int status;

    status = libusb_control_transfer(devh,
        LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE,
        0x01,
        (addr >> 16) & 0xffff,
        addr & 0xffff,
        data,
        len,
        1000);

    if (status < 0) {
        if(g_print_libusb_error)
            fprintf(stderr, "Error sending control packet: %s\n", libusb_error_name(status));
        return -1;
    }
    else if (status != len) {
        if(g_print_libusb_error)
            fprintf(stderr, "Error invalid len: %d\n", status);
        return -2;
    }

    return 0;
}

static int aml_chipid(uint8_t* data)
{
    return aml_rreg(0xd900d400, 12, data);
}

uint32_t aml_rreg32r(uint32_t addr, int* ret);
uint32_t aml_rreg32(uint32_t addr)
{
    return aml_rreg32r(addr, NULL);
}

uint32_t aml_rreg32r(uint32_t addr, int* ret)
{
    uint32_t val = 0;

    if(ret)
        *ret = aml_rreg(addr, sizeof(val), &val);
    else
        aml_rreg(addr, sizeof(val), &val);

    return le32toh(val);
}

int aml_wreg32(uint32_t addr, uint32_t val)
{
    val = htole32(val);
    return aml_wreg(addr, sizeof(val), &val);
}

static int print_usage(char* procname, int error_code) {
    fprintf(stderr, "\nUsage: %s {fastboot,recovery,normal}\n", procname);
    return error_code;
}

static int set_reboot_mode(uint32_t mode)
{
    int ret;
    ret = aml_wreg32(AO_RTI_STATUS_REG3, mode);
    if(ret != 0)
    {
        fprintf(stderr, "Fail to write AO_RTI_STATUS_REG3 register. ret=%d\n", ret);
        return -1;
    }

    return 0;
}



static void reset_system(void)
{
    int i;

    //supress libusb error messages
    g_print_libusb_error = 0;

    for(i=0; i<100; i++) 
    {
        aml_wreg32(P_WATCHDOG_CNTL, 0x3 | (1 << 21) // sys reset en
                                              | (1 << 23) // interrupt en
                                              | (1 << 24) // clk en
                                              | (1 << 25) // clk div en
                                              | (1 << 26)); // sys reset now


        aml_wreg32(P_WATCHDOG_RESET, 0);

        aml_wreg32(P_WATCHDOG_CNTL, aml_rreg32(P_WATCHDOG_CNTL) | (1<<18)); // watchdog en

        usleep(1000);
    }
}

int main(int argc, char **argv)
{
    int i;
    int rc = 0;
    uint8_t chipid[12];
    uint32_t reboot_mode;

    if(argc < 2)
    {
        return print_usage(argv[0], 1);
    }

    if(strcmp(argv[1],"fastboot") == 0)
    {
        reboot_mode = AMLOGIC_FASTBOOT_REBOOT;
    }
    else if(strcmp(argv[1],"cold") == 0)
    {
        reboot_mode = AMLOGIC_COLD_BOOT;
    }
    else if(strcmp(argv[1],"normal") == 0)
    {
        reboot_mode = AMLOGIC_NORMAL_BOOT;
    }
    else if(strcmp(argv[1],"factory") == 0)
    {
        reboot_mode = AMLOGIC_FACTORY_RESET_REBOOT;
    }
    else if(strcmp(argv[1],"update") == 0)
    {
        reboot_mode = AMLOGIC_UPDATE_REBOOT;
    }
    else if(strcmp(argv[1],"bootloader") == 0)
    {
        reboot_mode = AMLOGIC_BOOTLOADER_REBOOT;
    }
    else if(strcmp(argv[1],"rpmbp") == 0)
    {
        reboot_mode = AMLOGIC_RPMBP_REBOOT;
    }
    else
    {
        fprintf(stderr, "unknow reboot mode\n");
        return print_usage(argv[0], 1);;
    }

    rc = libusb_init(NULL);
    if (rc < 0) {
        fprintf(stderr, "Error initializing libusb: %s\n", libusb_error_name(rc));
        return 1;
    }

    devh = libusb_open_device_with_vid_pid(NULL, 0x1b8e, 0xc003);
    if (!devh) {
        fprintf(stderr, "Error finding USB device\n");
        rc = 1;
        goto out;
    }

    rc = aml_chipid(chipid);
    if(rc < 0) {
        fprintf(stderr, "Fail to read ChipID\n");
        rc = 1;
        goto out;
    }
    
    printf("Chipid: ");
    for(i=0; i<sizeof(chipid); i++)
        printf("%02x", chipid[i]);
    printf("\n");

    printf("Set reboot mode\n");
    rc = set_reboot_mode(reboot_mode);
    if (rc < 0) {
        fprintf(stderr, "Fail to set reboot mode: %d\n", rc);
        rc = 1;
        goto out;
    }

    printf("Reset...\n");
    reset_system();

out:
    if (devh)
        libusb_close(devh);
    libusb_exit(NULL);
    return rc;
}
