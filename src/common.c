#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include "common.h"
#include "secure_apb.h"

void clrbits_le32(uint32_t addr, uint32_t val)
{
    uint32_t temp = aml_rreg32(addr);
    temp = temp & (~val);
    aml_wreg32(addr, temp);
}

void setbits_le32(uint32_t addr, uint32_t val)
{
    uint32_t temp = aml_rreg32(addr);
    temp = temp | val;
    aml_wreg32(addr, temp);
}

uint32_t readl(uint32_t addr)
{
    return aml_rreg32(addr);
}

void writel(uint32_t addr, uint32_t val)
{
    aml_wreg32(addr, val);
}

uint32_t get_timer(uint32_t x)
{
    long ms;
    struct timespec spec;

    clock_gettime(CLOCK_MONOTONIC, &spec);
    ms = spec.tv_nsec / 1000000l;

    return (uint32_t)ms - x;
}

cpu_id_t get_cpu_id(void)
{
	cpu_id_t cpu_id;
	unsigned int cpu_id_reg = readl(P_AO_SEC_SD_CFG8);

	cpu_id.family_id = (cpu_id_reg >> 24) & (0XFF);
	cpu_id.package_id = (cpu_id_reg >> 16) & (0XF0);
	cpu_id.chip_rev = (cpu_id_reg >> 8) & (0XFF);
	cpu_id.layout_ver = (cpu_id_reg) & (0XF);
	return cpu_id;
}

static bool debug_en;

void debug(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	if (debug_en)
		vprintf(fmt, args);
}

void set_debug(bool sw)
{
    debug_en = sw;
}

