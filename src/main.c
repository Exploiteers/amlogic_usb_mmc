#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>
#include <libusb-1.0/libusb.h>
#include <stdarg.h>
#include <getopt.h>
#include <endian.h>
#include "common.h"
#include "mmc.h"

static struct libusb_device_handle *devh = NULL;

static int aml_rreg(uint32_t addr, uint32_t len, void* data)
{
    int status;

    status = libusb_control_transfer(devh,
        LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE,
        0x02,
        (addr >> 16) & 0xffff,
        addr & 0xffff,
        data,
        len,
        1000);

    if (status < 0) {
        fprintf(stderr, "Error sending control packet: %s\n", libusb_error_name(status));
        return -1;
    }
    else if (status != len) {
        fprintf(stderr, "Error invalid len: %d\n", status);
        return -1;
    }

    return 0;
}

static int aml_wreg(uint32_t addr, uint32_t len, void* data)
{
    int status;

    status = libusb_control_transfer(devh,
        LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE,
        0x01,
        (addr >> 16) & 0xffff,
        addr & 0xffff,
        data,
        len,
        1000);

    if (status < 0) {
        fprintf(stderr, "Error sending control packet: %s\n", libusb_error_name(status));
        return -1;
    }
    else if (status != len) {
        fprintf(stderr, "Error invalid len: %d\n", status);
        return -1;
    }

    return 0;
}

static int aml_chipid(uint8_t* data)
{
    return aml_rreg(0xd900d400, 12, data);
}

uint32_t aml_rreg32(uint32_t addr)
{
    uint32_t val = 0;
    aml_rreg(addr, sizeof(val), &val);

    return le32toh(val);
}

void aml_wreg32(uint32_t addr, uint32_t val)
{
    val = htole32(val);
    aml_wreg(addr, sizeof(val), &val);
}

static int print_usage(char* procname, int error_code) {
    fprintf(stderr, "\nUsage: %s [-v] [-V] {-r,-w} -s START_BLOCK -c BLOCK_COUNT -f FILE\n", procname);
    fprintf(stderr, "  -f <path>       -- Output/input file\n");
    fprintf(stderr, "  -r              -- Read MMC\n");
    fprintf(stderr, "  -w              -- Write MMC\n");
    fprintf(stderr, "  -s              -- Start block\n");
    fprintf(stderr, "  -c              -- Block count\n");
    fprintf(stderr, "  -p              -- Switch MMC parition\n");
    fprintf(stderr, "  -V              -- Print program version\n");
    fprintf(stderr, "  -v              -- Verbose\n");
    fprintf(stderr, "  -h              -- Print help\n");
    return error_code;
}

enum commandop {CMD_NOP, 
                CMD_READ, 
                CMD_WRITE};

int main(int argc, char **argv)
{
    enum commandop op = CMD_NOP;
    int opt, i;
    int rc = 0;
    const char *filepath   = NULL;
    const char *startblock = NULL;
    const char *countblock = NULL;
    const char *switchpart = NULL;
    uint32_t start = 0;
    uint32_t count = 0;
    uint32_t part = 0;
    uint8_t chipid[12];
    
    while ((opt = getopt(argc, argv, "vVhrwp:c:s:f:")) != EOF) {
        switch(opt) {
        case 'h':
            return print_usage(argv[0], 0);
        case 'V':
            printf("%s Version: 1.0\n", argv[0]);
            return 0;
        case 'v':
            set_debug(true);
            break;
        case 'r':
            op = CMD_READ;
            break;
        case 'w':
            op = CMD_WRITE;
            break;
        case 's':
            startblock = optarg;
            break;
        case 'c':
            countblock = optarg;
            break;
        case 'f':
            filepath = optarg;
            break;
        case 'p':
            switchpart = optarg;
            break;
        default:
            fprintf(stderr, "Error: unknow opt: %c\n", (char)opt);
            return 1;
        }
    }
    
    if(op != CMD_NOP) {
        if(!startblock) {
            fprintf(stderr, "Error: missing start block\n");
            return print_usage(argv[0], 1);
        }
        if(!countblock) {
            fprintf(stderr, "Error: missing block count\n");
            return print_usage(argv[0], 1);
        }
        if(!filepath) {
            fprintf(stderr, "Error: missing file name\n");
            return print_usage(argv[0], 1);
        }
        
        start = (uint32_t)strtol(startblock, NULL, 0);
        count = (uint32_t)strtol(countblock, NULL, 0);
    }

    rc = libusb_init(NULL);
    if (rc < 0) {
        fprintf(stderr, "Error initializing libusb: %s\n", libusb_error_name(rc));
        return 1;
    }

    devh = libusb_open_device_with_vid_pid(NULL, 0x1b8e, 0xc003);
    if (!devh) {
        fprintf(stderr, "Error finding USB device\n");
        rc = 1;
        goto out;
    }

    rc = aml_chipid(chipid);
    if(rc < 0) {
        fprintf(stderr, "Fail to read ChipID\n");
        rc = 1;
        goto out;
    }
    
    printf("Chipid: ");
    for(i=0; i<sizeof(chipid); i++)
        printf("%02x", chipid[i]);
    printf("\n");

    printf("EMMC init:\n");
    mmc_initialize(NULL);
    struct mmc *mmc;
    mmc = find_mmc_device(CONFIG_SYS_MMC_ENV_DEV);

    if(switchpart) {
        part = (uint32_t)strtol(switchpart, NULL, 0);
        printf("Switching HW partition to: %d\n", part);

        if(mmc_switch_part(CONFIG_SYS_MMC_ENV_DEV, part) != 0)
        {
            fprintf(stderr, "ERROR: couldn't switch to boot partition\n");
            rc = 1;
            goto out;
        }
    }

    printf("Device: %s\n", mmc->cfg->name);
    printf("Manufacturer ID: %x\n", mmc->cid[0] >> 24);
    printf("OEM: %x\n", (mmc->cid[0] >> 8) & 0xffff);
    printf("Name: %c%c%c%c%c \n", mmc->cid[0] & 0xff,
          (mmc->cid[1] >> 24), (mmc->cid[1] >> 16) & 0xff,
          (mmc->cid[1] >> 8) & 0xff, mmc->cid[1] & 0xff);

    printf("Tran Speed: %d\n", mmc->tran_speed);
    printf("Rd Block Len: %d\n", mmc->read_bl_len);

    printf("%s version %d.%d\n", IS_SD(mmc) ? "SD" : "MMC",
          (mmc->version >> 8) & 0xf, mmc->version & 0xff);

    printf("High Capacity: %s\n", mmc->high_capacity ? "Yes" : "No");
    printf("Capacity: %lu B\n", mmc->capacity);
    printf("mmc clock: %u\n", mmc->clock);
    printf("Bus Width: %d-bit%s\n", mmc->bus_width,
            mmc->ddr_mode ? " DDR" : "");

    uint32_t blk = 0;
    FILE *f = NULL;
    uint8_t *buffer = NULL;
    
    switch(op) {
    case CMD_READ:
        f = fopen(filepath, "wb");
        if(!f) {
            fprintf(stderr, "Error cannot open output file: %s\n", filepath);
            rc = 1;
            goto out;
        }
        
        buffer = (uint8_t*)malloc(512);
        if(!buffer) {
            fprintf(stderr, "Error cannot malloc buffer\n");
            rc = 1;
            fclose(f);
            goto out;
        }
        
        for(blk=start;blk<start+count;blk++) {
            printf("Reading block: 0x%08x\r", blk);
            fflush(stdout);
            if(mmc->block_dev.block_read(CONFIG_SYS_MMC_ENV_DEV, blk, 1, buffer) != 1)
            {
                fprintf(stderr, "\nERROR: read block fail\n");
                rc = 1;
                fclose(f);
                goto out;
            }
            fwrite(buffer, 1, 512, f);
        }
        
        printf("\nFinish.\n");
        fclose(f);
        rc = 0;
        break;
    case CMD_WRITE:
        f = fopen(filepath, "rb");
        if(!f) {
            fprintf(stderr, "Error cannot open input file: %s\n", filepath);
            rc = 1;
            goto out;
        }
        
        buffer = (uint8_t*)malloc(512);
        if(!buffer) {
            fprintf(stderr, "Error cannot malloc buffer\n");
            rc = 1;
            fclose(f);
            goto out;
        }
        
        for(blk=start;blk<start+count;blk++) {
            rc = fread(buffer, 1, 512, f);
            if(rc != 512) {
                fprintf(stderr, "Error: file not block align.\n"
                                "Try to read: %d got back: %d\n"
                                "Terminate.\n", 512, rc);
                rc = 1;
                fclose(f);
                goto out;
            }
            
            printf("Writing block: 0x%08x\r", blk);
            fflush(stdout);
            if(mmc->block_dev.block_write(CONFIG_SYS_MMC_ENV_DEV, blk, 1, buffer) != 1)
            {
                fprintf(stderr, "\nERROR: write block fail\n");
                rc = 1;
                fclose(f);
                goto out;
            }
        }
        
        printf("\nFinish.\n");
        fclose(f);
        rc = 0;
        break;
    default:
        printf("No operation\n");
        rc = 0;
    }
    
out:
    if (devh)
        libusb_close(devh);
    libusb_exit(NULL);
    return rc;
}
